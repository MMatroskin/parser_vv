BASE_URL = 'https://volveter.ru/sitemap-iblock-18.xml'
HEADERS = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0', 'accept': '*/*'}
HOST = 'https://volveter.ru'
DATA_ITEMS = [
    'name',
    'main_img',
    'color',
    'options',
    'features',
    'desc_short',
    'desc_detail',
    'price',
    'price_discount',
    'documents',
    'url'
]
SEP_H = '\r\n'
RESULT_FILE = 'results.csv'
