# from time import sleep
from config import *
from parser_srv.RequestSrv import RequestSrv
from parser_srv.XmlItem import XmlItem
from parser_srv.PageItem import PageItem
from parser_srv.CsvItem import CsvItem


def main(*args):

    if len(args) > 0:
        link_list = args[0]
    else:
        html = RequestSrv.get_html(BASE_URL, HEADERS)
        if html.status_code != 200:
            print('Connection error!')
        else:
            xml = XmlItem(url=html.url, text=html.text)
            link_list = xml.links
    if len(link_list) > 0:
        link_errors = []
        result = []
        count = 0
        print('Processing')
        for i in link_list:
            html_item = RequestSrv.get_html(i, HEADERS)
            if html_item.status_code == 200:
                item = PageItem(html_item.text, DATA_ITEMS, url=html_item.url, host=HOST, sep_h_rem=SEP_H)
                if item.is_item_page():
                    count += 1
                    print(str(count), end='\t')
                    item.parse()
                    result.append(item.data)
                    print(item.data['name'], item.success.message, sep='\t')
            else:
                link_errors.append(i)
            # sleep(2)
        if len(link_errors) > 0:
            print('\nErrors:')
            for i in link_errors:
                print(i)
        print('...')
        csv = CsvItem(DATA_ITEMS, RESULT_FILE)
        csv.set_data(result)
        csv.save_data()
    print('Done')


if __name__ == '__main__':
    main()
