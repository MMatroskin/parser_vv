import requests


class RequestSrv:

    @staticmethod
    def get_html(url, headers):
        result = requests.get(url, headers=headers, params=None)
        return result

    @staticmethod
    def get_html_async(url):
        pass
