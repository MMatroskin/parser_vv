from xml.dom.minidom import parseString


class XmlItem:
    url = None
    links = []

    def __init__(self, **data):
        self.url = data.get('url')
        dom = parseString(data.get('text'))
        node_list = dom.getElementsByTagName('loc')
        tmp = set()
        for i in node_list:
            if len(i.childNodes) < 2:
                node = i.childNodes[0]
                value = self.get_node_data(node)
                if value is not None:
                    tmp.add(value)
        self.links = sorted(tmp)

    @staticmethod
    def get_node_data(node):
        result = None
        if node.nodeType == node.TEXT_NODE:
            result = node.data
        return result
