from bs4 import BeautifulSoup
import re
import json
from .ReportItem import ReportItem


class PageItem:
    _content = None
    _sep_h = '\n'
    _sep_w = ':\t'
    _sep_w_table = '\t'
    _list_item_marker = '\t'
    url = None
    data = dict()

    def __init__(self, content, data_items, **data):

        self.success = ReportItem()
        self._host = data.get('host')
        self._sep_h_old = data.get('sep_h_rem')

        soup = BeautifulSoup(content, 'html.parser')
        self._content = soup.find('div', {'class': 'catalog_detail detail element_custom'})

        self.data = dict.fromkeys(data_items, '')
        self.data['name'] = soup.h1.text
        self.data['url'] = data.get('url')

    def is_item_page(self):
        result = False
        if self._content is not None:
            result = True
        return result

    def parse(self):
        try:
            desc_short_container = self._content.find('div', {'class': 'preview_text dotdot'})
            if desc_short_container is not None:
                self.data['desc_short'] = self._normalize_line(desc_short_container.text)

            main_img_container = self._content.find('div', {'class': 'offers_img'})
            if main_img_container is None:
                main_img_container = self._content.find('div', {'class': 'item_slider'})
            main_img_item = main_img_container.find('a', {'class': 'popup_link'})
            if main_img_item is not None:
                self.data['main_img'] = self._host + main_img_item.attrs['href']

            price_container = self._content.find(lambda tag:
                                                  tag.name == 'div' and
                                                  tag.attrs.get('class') == ['price'])
            if price_container is not None:
                try:
                    self.data['price'] = price_container.contents[1].text
                except Exception:
                    pass

            price_discount_container = self._content.find(lambda tag:
                                                  tag.name == 'div' and
                                                  tag.attrs.get('class') == ['price', 'discount'] and
                                                  tag.text != '')
            if price_discount_container is not None:
                try:
                    self.data['price_discount'] = price_discount_container.text.replace('\n', '')
                except Exception:
                    pass

            colors_container = self._content.find('div', {'class': 'bx_scu'})
            if colors_container is not None:
                color_items = self._get_list_content(colors_container)
                tmp = self._to_json(color_items, ': ')
                self.data['color'] = self._normalize_line(tmp)

            options_container = self._content.find('div', {'class': 'bx_size'})
            if options_container is not None:
                options = self._get_list_content(options_container, is_simple=True)
                tmp = self._to_json(options, ': ')
                self.data['options'] = tmp

            desc_detail_container = self._content.find('div', {'class': 'detail_text'})
            if desc_detail_container is not None:
                items_detail_data = []
                items_detail = desc_detail_container.contents
                for i in items_detail:
                    line = None
                    try:
                        line = self._normalize_line(i)
                    except Exception:
                        try:
                            if i.name == 'ul':
                                tmp = self._get_list_content(i, is_simple=True, add_marker=True)
                                line = self._sep_h.join(tmp)
                            else:
                                table_container = i
                                if table_container.name != 'table':
                                    table_container = i.find('table')
                                if table_container is not None:
                                    tmp = self._get_table_content(table_container, is_horizontal=True)
                                    line = self._sep_h.join(tmp)
                                else:
                                    line = self._normalize_line(i.text)
                        except Exception:
                            pass
                    if line is not None and len(line) > 0:
                            items_detail_data.append(line)
                self.data['desc_detail'] = self._sep_h.join(items_detail_data)  # multiline

            features_container = self._content.find('table', {'class': 'props_list'})
            if features_container is not None:
                features_items = self._get_table_content(features_container)
                tmp = self._to_json(features_items, ':\t', single_val=True)
                self.data['features'] = tmp  # multiline

            docs_container = self._content.find('div', {'class': 'files_block'})
            if docs_container is not None:
                docs_links = docs_container.findAll('a', {'class': 'dark_link'})
                if docs_links is not None:
                    docs_data = []
                    for i in docs_links:
                        try:
                            doc_link = self._host + i.attrs['href']
                            docs_data.append(doc_link)
                        except:
                            pass
                    self.data['documents'] = self._sep_h.join(docs_data)  # multiline
        except Exception as e:
            self.success.success = False
            self.success.message = str(e)

    def _get_list_content(self, container, is_simple=False, add_marker=False):
        result = []
        if container.name == 'ul':
            item = container
        else:
            item = container.find('ul')
        if item is not None:
            for i in item.contents:
                try:
                    if is_simple:
                        try:
                            line = self._normalize_line(i.attrs.get('title'))
                        except Exception:
                            line = self._normalize_line(i.text)
                        if add_marker:
                            line = self._list_item_marker + line
                    else:
                        line = i.contents[0].attrs.get('title')
                    if len(line) > 0:
                        result.append(line)
                except Exception:
                    pass
        return result

    def _get_table_content(self, item, is_horizontal=False):
        result = []
        try:
            tmp = []
            subitems = item.findAll('tr')
            for i in subitems:
                value = []
                for j in i.contents:
                    if j.name == 'td' or j.name == 'th':
                        value.append(self._normalize_line(j.text))
                if len(value) > 0:
                    tmp.append(value)
            if is_horizontal:
                sep_w = self._sep_w_table
                for i in range(len(tmp[0])):
                    items = []
                    for j in range(len(tmp)):
                        items.append(tmp[j][i])
                    result.append(sep_w.join(items))
            else:
                sep_w = self._sep_w
                for i in range(len(tmp)):
                    result.append(sep_w.join(tmp[i]))
        except Exception:
            pass
        return result

    def _normalize_line(self, line):
        result = re.sub('\t{2,}', '\t', line)
        if self._sep_h_old is not None:
            result = result.replace(self._sep_h_old, self._sep_h)
        result = re.sub('\n{2,}', '\n', result)
        result = re.sub(r'«|»', r'"', result)
        result = self._clear_start_end(result)
        return result

    @staticmethod
    def _clear_start_end(line, item='\n'):
        result = line.strip()
        if result.startswith(item):
            result = result[1: len(result)]
        if result.endswith(item):
            result = result[0: len(result) - 1]
        result = result.strip()
        return result

    def _to_json(self, lst, sep, single_val=False):
        tmp_list = []
        values = []
        current_key = None
        for i in lst:
            tmp = i.split(sep)
            if tmp[0] != current_key:
                if current_key is not None:
                    tmp_list = self._add_values_toList(tmp_list, current_key, values, single_val)
                values.clear()
                current_key = tmp[0]
            values.append(tmp[1])
        tmp_list = self._add_values_toList(tmp_list, current_key, values, single_val)
        result = json.dumps(tmp_list, separators=(',', ':'), ensure_ascii=False)
        return result

    @staticmethod
    def _add_values_toList(lst, key, values, single_val):
        value = values[0]
        if not single_val:
            value = values.copy()
        lst.append({key: value})
        return lst
