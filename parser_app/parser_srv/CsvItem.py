import os
import csv


class CsvItem:
    _data = []

    def __init__(self, items, filename, dir=''):
        if dir == '' or not os.path.exists(dir):
            dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self._dir = dir
        self._name = filename
        self._items = items
        self._tabs = items.copy()
        self._tabs.insert(0, 'NN')


    def set_data(self, data):
        line_list = []
        count = 0
        for item in data:
            count += 1
            tmp_list = [str(count)]
            for i in self._items:
                tmp_list.append(item.get(i))
            line_list.append(tmp_list)
        self._data = line_list

    def save_data(self):
        lines = self._data
        lines.insert(0, self._tabs)
        file = os.path.join(self._dir, self._name)
        with open(file, 'wt', newline='', encoding='utf-8') as fh:
            writer = csv.writer(fh)
            writer.writerows(lines)
